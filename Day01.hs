fuelByMass :: Int -> Int
fuelByMass = subtract 2 . (`div` 3)

extraFuel :: Int -> Int
extraFuel = sum . takeWhile (> 0) . iterate fuelByMass

main :: IO ()
main = do
  masses <- fmap read . lines <$> readFile "Day01.in"
  print . sum . map fuelByMass $ masses
  print . sum . map (extraFuel . fuelByMass) $ masses