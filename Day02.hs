import Data.List.Split (splitOn)
import Data.List (find)

tests = [
  ([1,0,0,0,99], [2,0,0,0,99]),
  ([2,3,0,3,99], [2,3,0,6,99]),
  ([2,4,4,5,99,0], [2,4,4,5,99,9801]),
  ([1,1,1,4,99,5,6,0,99], [30,1,1,4,2,5,6,0,99])]

runTests = fmap (\(i, o) -> runIntcode 0 i == o) tests

replaceAt :: Int -> Int -> [Int] -> [Int]
replaceAt i v = (\(l, _ : r) -> l ++ v : r) . splitAt i

runIntcode :: Int -> [Int] -> [Int]
runIntcode pc instr = case drop pc instr of
  1 : rest -> runIntcode (pc + 4) (apply (+) rest)
  2 : rest -> runIntcode (pc + 4) (apply (*) rest)
  99 : _ -> instr
  op : _ -> error (show op ++ " " ++ show pc)
  where
    apply op (l:r:dest:_) = replaceAt dest (op (instr !! l) (instr !! r)) instr

main :: IO ()
main = do
  instructions <- fmap read . splitOn "," <$> readFile "Day02.in"
  let input one two = replaceAt 2 two . replaceAt 1 one $ instructions
  putStr "Part one: "
  print . head . runIntcode 0 $ input 12 2
  putStr "Part two: "
  let possibleInputs = (,) <$> [0..99] <*> [0..99]
  print . fmap (\(one, two) -> one * 100 + two)
        . find ((==) 19690720 . head . runIntcode 0 . uncurry input)
        $ possibleInputs
