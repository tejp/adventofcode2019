import Data.HashSet (fromList, toList, intersection)
import Data.List.Split (splitOn)
import Data.List (elemIndex)

tests = [
  ("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", 159),
  ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135)]

parseWire :: String -> [(Int, Int)]
parseWire = scanl1 sumPoints . concatMap parseDir . splitOn ","
  where
    sumPoints (x, y) (dx, dy) = (x + dx, y + dy)
    parseDir (dir:number) = replicate (read number) $ case dir of
      'U' -> (0, 1)
      'R' -> (1, 0)
      'D' -> (0, -1)
      'L' -> (-1, 0)

main :: IO ()
main = do
  [wire1, wire2] <- fmap parseWire . lines <$> readFile "Day03.in"
  let intersections = toList $ intersection (fromList wire1) (fromList wire2)
  putStr "Part one: "
  print . minimum $ fmap distance intersections
  putStr "Part two: "
  print . minimum $ fmap (delay wire1 wire2) intersections
  where
    distance (x, y) = abs x + abs y
    delay w1 w2 x_y = (+) . (+2) <$> elemIndex x_y w1 <*> elemIndex x_y w2
