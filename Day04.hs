import Data.List (sort, group)

partOne, partTwo :: Ord a => [a] -> Bool
partOne ds = any ((>=2) . length) (group ds) && sort ds == ds
partTwo ds = any ((==2) . length) (group ds) && sort ds == ds

main :: IO ()
main = do
  let input = [246515..739105]
  putStr "Part one: "
  print . length $ filter (partOne . show) input
  putStr "Part two: "
  print . length $ filter (partTwo . show) input
