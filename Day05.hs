import Data.List.Split (splitOn)
import qualified Data.Vector.Unboxed as V

import Intcode

main :: IO ()
main = do
  program <- V.fromList . fmap read . splitOn "," <$> readFile "Day05.in"
  putStrLn $ "Part one: " <> show (runOpcode [1] program)
  putStrLn $ "Part two: " <> show (runOpcode [5] program)
