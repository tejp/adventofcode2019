import qualified Data.HashMap.Strict as M
import Data.List (intersect, elemIndex)
import Safe (headMay)

getParents :: M.HashMap String String -> String -> [String]
getParents m s = s : maybe [] (getParents m) (M.lookup s m)

main :: IO ()
main = do
  relations <- M.fromList . fmap parseRel . lines <$> readFile "Day06.in"
  let parents = fmap (getParents relations) relations
  putStr "Part one: "
  print . sum $ fmap length parents
  putStr "Part two: "
  print $ do
    pYou <- M.lookup "YOU" parents
    pSan <- M.lookup "SAN" parents
    common <- headMay $ intersect pYou pSan
    (+) <$> elemIndex common pYou <*> elemIndex common pSan
  where
    parseRel s = (drop 4 s, take 3 s)
