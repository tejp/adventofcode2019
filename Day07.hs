import Data.List.Split (splitOn)
import qualified Data.Vector.Unboxed as V
import Data.List (permutations)

import Intcode

main :: IO ()
main = do
  program <- V.fromList . fmap read . splitOn "," <$> readFile "Day07.in"
  let runPipe phase lastOutput =
        runOpcode (phase:lastOutput) program
      withoutFeedback phase =
        head $ foldr runPipe [0] phase
      withFeedback phase =
        last output where output = foldr runPipe (0:output) phase
  putStr "Part one: "
  print $ maximum [(withoutFeedback p, p) | p <- permutations [0..4]]
  putStr "Part two: "
  print $ maximum [(withFeedback p, p) | p <- permutations [5..9]]
