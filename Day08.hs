import Data.List.Split (chunksOf)
import Data.List (minimumBy, transpose)
import Data.Char (isDigit, digitToInt)
import Data.Ord (comparing)
import Data.Map (fromListWith, elems)

main :: IO ()
main = do
  digits <- fmap digitToInt . filter isDigit <$> readFile "Day08.in"
  let layers = chunksOf (25*6) digits

  putStr "Part one: "
  let countDistinct = elems . fromListWith (+) . map (\d -> (d, 1))
  print .
    (\[_, ones, twos] -> ones * twos) .
    minimumBy (comparing head) $
    fmap countDistinct layers

  putStrLn "Part two: "
  let decodeImage layers = head . dropWhile (==2) <$> transpose layers
      showImage = unlines . chunksOf 25 . fmap showPixel
      showPixel pixel = if pixel == 1 then '#' else ' '
  putStrLn . showImage $ decodeImage layers
