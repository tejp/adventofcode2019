import Data.List.Split (splitOn)
import qualified Data.Vector.Unboxed as V

import Intcode

main :: IO ()
main = do
  program <- V.fromList . fmap read . splitOn "," <$> readFile "Day09.in"
  putStr "Part one: "
  print $ runOpcode [1] (program <> V.replicate 57 0)
  putStr "Part two: "
  print $ runOpcode [2] (program <> V.replicate 104 0)
