import qualified Data.Map as M

asteroidsXY :: String -> [(Int, Int)]
asteroidsXY s = do
  (cs, y) <- zip (lines s) [0..]
  (c , x) <- zip cs [0..]
  [(x, y) | c == '#']

asteroidAngles :: (Int, Int) -> [(Int, Int)] -> M.Map Double [(Int, Int)]
asteroidAngles (x, y) =
  M.fromListWith (<>) .
  fmap (\a -> (angle $ wayTo a, [a])) .
  filter (/= (x, y))
  where
    wayTo (a, b) = (a - x, b - y)
    angle (a, b) =
      atan2 (fromIntegral a) (fromIntegral (-b)) + if a < 0 then 2 * pi else 0

main :: IO ()
main = do
  asteroids <- asteroidsXY <$> readFile "Day10.in"
  let angles a = let as = asteroidAngles a asteroids in (length as, as, a)
      (n, as, (x, y)) = maximum $ fmap angles asteroids
  putStr "Part one: "
  print (n, (x, y))
  putStr "Part two: "
  let distance (a, b) = (sqrt $ fromIntegral ((a - x)^2 + (b - y)^2), a * 100 + b)
  print . snd . minimum . fmap distance . snd $ M.elemAt 199 as
