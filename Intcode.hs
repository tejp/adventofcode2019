module Intcode where

import Data.Vector.Unboxed (Vector, (!), (//))
import Data.Bool (bool)

runOpcode :: [Int] -> Vector Int -> [Int]
runOpcode = run 0 0 where
  run pc rBase input ops = case op of
    01 -> run (pc + 4) rBase input (store 3 (load 1 + load 2))
    02 -> run (pc + 4) rBase input (store 3 (load 1 * load 2))
    03 -> run (pc + 2) rBase (tail input) (store 1 (head input))
    04 -> load 1 : run (pc + 2) rBase input ops
    05 -> run (bool (pc + 3) (load 2) (load 1 /= 0)) rBase input ops
    06 -> run (bool (pc + 3) (load 2) (load 1 == 0)) rBase input ops
    07 -> run (pc + 4) rBase input (store 3 (bool 0 1 (load 1 < load 2)))
    08 -> run (pc + 4) rBase input (store 3 (bool 0 1 (load 1 == load 2)))
    09 -> run (pc + 2) (rBase + load 1) input ops
    99 -> []
    where
      (modes, op) = (ops ! pc) `divMod` 100
      load = (ops !) . address
      store i v = ops // [(address i, v)]
      address i = case modes `div` (10^(i-1)) `mod` 10 of
        0 -> ops ! (pc + i)
        1 -> pc + i
        2 -> (rBase +) $ (ops !) $ pc + i
